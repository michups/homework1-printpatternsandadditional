package com.michups.additionalexercises;

/**
 * Created by michups on 24.05.17.
 */
public class PrintPatterns {
    public static void main(String[] args) {

        int width = 8;

        patternA(width);
        System.out.println("----------");

        patternB(width);
        System.out.println("----------");

        patternC(width);
        System.out.println("----------");

        patternD(width);
        System.out.println("----------");

        patternE(width);
        System.out.println("----------");

        patternF(width);
        System.out.println("----------");

        patternG(width);
        System.out.println("----------");

        patternH(width);
        System.out.println("----------");

        patternI(width);
        System.out.println("----------");

        patternJ(width);
    }

    public static void patternA(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( i <= j )
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
    public static void patternB(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( i >= (width-j) )
                    System.out.print(" ");
                else
                    System.out.print("#");
            }
            System.out.println();
        }
    }

    public static void patternC(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( i < j )
                    System.out.print(" ");
                else
                    System.out.print("#");
            }
            System.out.println();
        }
    }
    public static void patternD(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( i >=  (width-j-1) )
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
    public static void patternE(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( j==0 || j==width-1 || i==0 || i==width-1 )
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
    public static void patternF(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( j==0 || j==width-1 || i==j )
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
    public static void patternG(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( j==0 || j==width-1 || i==width-1-j )
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
    public static void patternH(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( j==0 || j==width-1 || i==width-1-j || i==j )
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
    public static void patternI(int width) {
        for (int j = 0; j < width; j++) {
            for (int i = 0; i < width; i++) {
                if ( j==0 || j==width-1 || i==width-1-j || i==j  || i==0 || i==width-1 )
                    System.out.print("#");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
    public static void patternJ(int width) {
        for (int j = 0; j <= width+1; j++) {
            for (int i = 0; i < width+1; i++) {
                if(i*(j-1)<10 && j!=1)
                    System.out.print(" ");

                if ( j==0 && i==0)
                    System.out.print("* |");
                else if ( j==0)
                    System.out.print(" " + i );
                else if ( j==1)
                    System.out.print("---");
                else if(j>1 && i ==0)
                    System.out.print(j-1 + " | ");
                else
                    System.out.print(i*(j-1) + " ");
            }
            if ( j==1)
                System.out.print("-");
            System.out.println();
        }
    }
}