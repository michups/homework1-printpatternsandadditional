package com.michups.additionalexercises;

/**
 * Created by michups on 22.05.17.
 */
public class ArrayIteration {
    public static void main(String[] args) {
        int testArray[] =  {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        //a all numbers
        System.out.print("a) ");
        for (int number:testArray) {
            System.out.print(number + " ");
        }
        System.out.println("");

        //b 6 first numbers
        System.out.print("b) ");
        for (int i=0; i<6 && i<testArray.length; i++) {
            System.out.print(testArray[i] + " ");
        }
        System.out.println("");

        //c 6 last numbers
        System.out.print("c) ");
        for (int i=testArray.length-6;  i<testArray.length ; i++) {
            System.out.print(testArray[i] + " ");
        }
        System.out.println("");


        //d all even numbers
        System.out.print("d) ");
        for (int i=0; i<testArray.length; i++) {
            if(testArray[i]%2==0)
            System.out.print(testArray[i] + " ");
        }
        System.out.println("");


        //e all numbers from end
        System.out.print("e) ");
        for (int i=testArray.length-1; i>=0; i--) {
            System.out.print(testArray[i] + " ");
        }
        System.out.println("");


        //f all numbers except for 5
        System.out.print("f) ");
        for (int i=0; i<testArray.length; i++) {
            if(testArray[i]!=5)
                System.out.print(testArray[i] + " ");
        }
        System.out.println("");



        //f all numbers to number  5
        System.out.print("g) ");
        for (int i=0; i<testArray.length; i++) {
                System.out.print(testArray[i] + " ");
            if(testArray[i]==5)
                break;
        }
        System.out.println("");



    }
}
