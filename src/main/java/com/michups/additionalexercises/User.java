package com.michups.additionalexercises;

/**
 * Created by michups on 22.05.17.
 */

public class User{
    private String login;
    private String password;

    public User(String login, String password){
        this.password=password;
        this.login = login;
    }

    public void printUser(){
        System.out.print( login+" ");
        if(password.length()<8)
            System.out.println("błędne hasło");
        else
            for (int i=0; i<password.length(); i++)
                System.out.print("*");
        System.out.println();

    }


    public static void main(String[] args) {

        User user1 = new User("heniek", "ala123");
        User user2 = new User("janusz", "sekretnehaslojanusza");

        user1.printUser();
        user2.printUser();
    }
}