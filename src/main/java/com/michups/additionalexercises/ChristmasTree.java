package com.michups.additionalexercises;

/**
 * Created by michups on 22.05.17.
 */
public class ChristmasTree {
    public static void main(String[] args) {
        int width=4;
        printMyTree(width);



    }


    public static void printMyTree(int width){
        int size = (width-1)*2+1;
        for (int j=0; j<width; j++){
            for (int i=0; i<=size; i++){
                if(i>=size/2-j && i<=size/2+j)
                    System.out.print("*");
                else
                    System.out.print(" ");
            }
            System.out.println();
        }
    }
}
